# Bienvenido a Random API

Esta API le permitirá obtener el número pi con una aproximación decimal al azar, basada en un numero entero de su elección.

Para esto se disponibiliza el endpoint {url}/pi?random_limit=<valor>, donde "<valor> es el placeholder del numero a ingresar

## Instrucciones básicas de instalación

Para operar la aplicación sin contratiempos se recomienda el uso de Docker.

1. Descargue e instale la distribución de Docker adecuada para su sistema operativo en https://www.docker.com/products/docker-desktop

2. Descargue Postman en https://www.postman.com/downloads/

3. Clone este repositorio en una carpeta de su equipo utilizando git clone https://gitlab.com/rodrigo.gtr/randompi.git

4. En la barra de comandos, navegue hasta la carpeta donde se encuentra el proyecto, y en el directorio raíz ejecute: 
   docker build --tag randompi:1.0 . (no olvide colocar el "." al final)

5. La instrucción anterior creó la imagen del proyecto. A continuación jecute una instancia de esta ingresando lo siguiente
   en la barra de comando: docker run -p 8000:8000 randompi:1.0

6. Para consultar un valor, utilizando Postman () haga un GET request a la siguiente dirección: 
   http://localhost:8000/pi?random_limit=<valor a consultar> (reemplazar <valor a consultar> con un numero entero positivo)

7. Se provee también archivo json con endpoint habilitado, la cual debe importarse con Postman para su uso
