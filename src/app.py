from os import environ
from flask import Flask, request, jsonify
import math
import random
import re

app = Flask(__name__)

@app.route('/pi', methods=['GET'])
def piRand():
    randomQuery = request.args.get('random_limit')
    validator = re.search('^[0-9]*$', randomQuery)
    if(validator):
        random_limit = int(randomQuery)
        decim = randNum(random_limit)
        piCalc = '{0:.{decim}f}'.format(math.pi, decim=decim)
        return {'PiCalc': piCalc}
    else:
        return jsonify({"Error": "Please enter only numbers!"})

def randNum(num):
    return random.randint(int(num/2), num)


if __name__ == '__main__':
    app.run()  
