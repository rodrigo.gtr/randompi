import json
import random
import decimal
import math
from ..app import randNum

def test_pi(app, client):
    testEntry = 10
    url = "/pi?random_limit={}".format(testEntry)
    res = client.get(url)
    decim = randNum(testEntry)
    assert res.status_code == 200
    assert decim >= 5, "decimal is between 5 and 10"
    assert decim <= 10, "decimal is between 5 and 10"


    piTenDecimals = [1,4,1,5,9,2,6,5,3,5]
    responseValue = json.loads(res.get_data(as_text=True))
    d = decimal.Decimal(responseValue["PiCalc"])
    returnedDecimal = d.as_tuple().exponent*(-1)
    currentDecim = []
    i = 0
    while i < returnedDecimal:
        currentDecim.append(piTenDecimals[i])
        i = i+1

    x = ''.join([str(x) for x in currentDecim])

    #expected = {"PiCalc": "3.{}".format(x)}
    #assert expected == json.loads(res.get_data(as_text=True))
    assert float("3.{}".format(x)) <= float(responseValue["PiCalc"])
    #TODO: math.pi aproximates last digit, making unable to compare returned dictionary to static piTenDecimals array
